package com.ukefu.webim.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.AgentUserTask;

public abstract interface AgentUserTaskRepository  extends JpaRepository<AgentUserTask, String>{
	
	public abstract AgentUserTask findById(String id);
	
}

