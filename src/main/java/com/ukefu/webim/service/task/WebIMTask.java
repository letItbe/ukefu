package com.ukefu.webim.service.task;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class WebIMTask {
	
	@Scheduled(fixedDelay= 5000) // 每5秒执行一次
    public void task() {
		
	}
}
